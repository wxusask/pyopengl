from OpenGL.GL import *
from OpenGL.GLU import *

import pygame
from pygame.locals import *

from mesh_generator import Mesh
from simulator import Simulator
from Constants import Constants



def render(mesh):
    m = mesh.positions.reshape(Constants.m, 3)
    glBegin(GL_POINTS)
    for i in range(Constants.m):
        glVertex3fv((m[i][0], m[i][1], m[i][2]))
    glEnd()


def setup_scene():
    pygame.init()
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)
    
    gluPerspective(45, (display[0] / display[1]), 0.1, 50.0)
    glTranslatef(0, 0, -5)
    


#-----------------Main function----------------------#
def main():
    setup_scene()
    mesh = Mesh()
    mesh.init_mesh()
    simulator = Simulator(mesh, Constants.h)
    
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    glRotatef(25, -0.5, 0, 1)
                if event.key == pygame.K_RIGHT:
                    glRotatef(25, 0.5, 0, 1)

                if event.key == pygame.K_UP:
                    glTranslatef(0, 10, 0)
                if event.key == pygame.K_DOWN:
                    glTranslatef(0, -10, 0)
                    
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        #code to render mesh
        render(mesh)
        #use simulator to step forward
        simulator.step()
        pygame.display.flip()


if __name__ == '__main__':
    main()