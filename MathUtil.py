# The endpoints and strings (3x3 grid) layout are depicted as follows:

# p(2,0) -- s(10) --p(2,1) -- s(11) -- p(2,2)
# |                  |                  |
# s(7)               s(8)              s(9)
# |                  |                  |
# p(1,0) -- s(5) -- p(1,1) -- s(6) -- p(1,2)
# |                  |                  |
# s(2)               s(3)              s(4)
# |                  |                  |
# p(0,0) -- s(0) -- p(0,1) -- s(1) -- p(0,2)

from Constants import Constants

def convert_string_to_endpoint(s, N=Constants.N):
    # Given string index s, return endpoint indices as ((x1, y1), (x2, y2))
    # NOTE: ENDPOINTS AND STRING INDICIES ALL START FROM 0
    M = (N-1)+N
    _r, _l = divmod(s, M)
    
    if _l < N-1:
        return (_r, _l), (_r, _l+1)
    else:
        _l = _l - (N-1)
        return (_r, _l), (_r+1, _l)
    
    
def convert_2D_coord_to_1D_coord(x, y, N=Constants.N):
    # Convert string endpoint 2D coordinate to 1D array indices
    # Note: 1D array indices starts from 0 NOT 1
    return x*N+y
    