import scipy as sp
import numpy as np
from Constants import Constants
import MathUtil

class Mesh:
    
    # Two fixed points
    fixed_pt1 = np.array([-1, 2, 0]).reshape((3, 1))
    fixed_pt2 = np.array([1, 2, 0]).reshape((3, 1))
    
    def __init__(self):
    # Gnerate a 80*80 square mesh
        self.positions = None
        self.velocities = None
        self.springs = None
        self.mass = None
        self.rest_length = 2 / (Constants.N-1)
        
    def init_mesh(self):
        self.init_positions()
        self.init_velocities()
        self.init_mass()


    # Give the mesh a special setting to start the simulation
    def set_mesh_props(self):
        pass
        
        
    # Positions: (p1_x, p1_y, p1_z, ..., pm_x, pm_y, pm_z)
    # Positions are use for rendering particles
    def init_positions(self):
        x_coord = np.linspace(-1, 1, Constants.N)
        y_coord = np.linspace(-1, 1, Constants.N)
        z_coord = np.zeros((Constants.m, 1))
        xy = np.array(np.meshgrid(x_coord, y_coord, indexing='ij')).T.reshape(-1, 2)
        self.positions = np.concatenate((xy, z_coord), axis=1)
        self.positions.resize([Constants.m * 3, 1])
        pass


    # Velocities: (p1_vx, p1_vy, p1_vz, ..., pm_vx, pm_vy, pm_vz)
    def init_velocities(self):
        self.velocities = np.zeros((Constants.m * 3, 1))

    
    def init_mass(self):
        self.mass = Constants.total_mass / Constants.m * np.eye((Constants.m * 3))
    

