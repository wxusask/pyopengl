from mesh_generator import Mesh
import MathUtil
from simulator import Simulator
from Constants import Constants

import numpy as np
import scipy as sp
import scipy.linalg as la

# A trivial test class for my dev use

def TestMesh():
    # m = Mesh()
    # m.init_mesh()
    # m.positions
    arr = np.array([1, 2, 3])
    for a in arr:
        print(a)




if __name__ == '__main__':
    
    
    mesh = Mesh()
    mesh.init_mesh()
    simulator = Simulator(mesh, Constants.h)
    simulator.step()
    
    pass