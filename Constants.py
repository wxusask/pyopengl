
class Constants:
    
    
    
    # Practical test
    # N = 80 # 2D mesh size
    # m = 6400 # 6400 particles
    # s = 12640 # Number of strings, for N=80: (79 + 80) * 79 + 79 = 12640
    
    # Small test
    N = 3 # 2D mesh size 3x3
    m = 9 # 9 particles
    s =  12# Number of strings (12)
    # Small test2
    # N = 2 # 2D mesh size 2x2
    # m = 4 # 4 particles
    # s = 4 # Number of strings 4
    #
    # Alternative test
    # k = 1# Spring stiffness k
    # N = 5 # 2D mesh size 5x5
    # m = 25 # 25 particles
    # s = 28 # Number of strings
    #

    k = 80  # Spring stiffness k
    gravity = 1.0
    total_mass = 1.0
    h = 0.0333  # step size
    max_iterations = 5 # maximum iterations
    damping = 1
    
    def __init__(self):
        #TODO cross section of the cloth is usually ~1e10-2 cm^2
        #TODO Young's module should be ~2e10^7 dyne/cm^2
        pass