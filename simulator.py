import numpy as np
import scipy as sp
import scipy.linalg as scipy_linalg
from mesh_generator import Mesh
from Constants import Constants

import MathUtil

class Simulator:
    
    #TODO: for now we only consider gravity and Young's module.
    #TODO: add two springs to simulate what is in the demo
    
    def __init__(self, mesh, h):
        self.L, self.J = self.compute_L_and_J_matrix()
        self.mesh = mesh
        self.A = self.mesh.mass + h*h*self.L
        self.cho_res = scipy_linalg.cho_factor(self.A)
        self.h = h
        self._d = np.zeros((Constants.s*3, 1))
        self.b = np.zeros((Constants.m*3, 1))
        self.y = np.zeros((Constants.m*3, 1))
        self.f_ext = np.zeros((Constants.m*3, 1))
        self._construct_fext_vector()
        self.next_pos = np.zeros((Constants.m*3, 1))
 
    
    # The optimization problem is
    # g(x) = 1/2 xT(M+h2L)x - h2xTJd + xT[-My + h2f], where y is the inertia and f is external force
    # Global step: compute Ax = b, where A = M + h2L, b = (h2Jd + My - h2f)
    # Local step: normalize torqued string to rest length
    def step(self):
        self.y = self.mesh.positions + self.h * Constants.damping * self.mesh.velocities
        self.next_pos = self.y[:]

        
        for i in range(Constants.max_iterations):
            self._local_step_for_d()
            #_debug = self.L.dot(self.mesh.positions) - self.J.dot(self._d)
            self._global_step_for_x()

        self.mesh.velocities = (self.next_pos - self.mesh.positions) / self.h
        self.mesh.positions = self.next_pos
    

    # Project d to desired direction and normalize it to rest length
    def _local_step_for_d(self):
        for i in range(Constants.s):
            endpoint1_idx, endpoint2_idx = MathUtil.convert_string_to_endpoint(i)
            pos1 = MathUtil.convert_2D_coord_to_1D_coord(*endpoint1_idx) * 3
            pos2 = MathUtil.convert_2D_coord_to_1D_coord(*endpoint2_idx) * 3
            direction = self.next_pos[pos1: pos1 + 3] - self.next_pos[pos2:pos2 + 3]
            self._d[i*3:i*3+3] = direction / np.linalg.norm(direction)*self.mesh.rest_length

    
    def _global_step_for_x(self):
        self.b = self.h * self.h * self.J.dot(self._d) + self.mesh.mass.dot(self.y)  + self.h*self.h*self.f_ext
        self.next_pos = scipy_linalg.cho_solve(self.cho_res, self.b)

      
    def _construct_fext_vector(self):
        #Gravity is applied to y-axis
        self.f_ext[1::3] = - Constants.total_mass/Constants.m * Constants.gravity


    def compute_L_and_J_matrix(self):
        L = np.zeros((Constants.m, Constants.m))
        J = np.zeros((Constants.m, Constants.s))
        for i in range(Constants.s):
            A_i = np.zeros((Constants.m, 1))
            endpoint_1, endpoint_2 = MathUtil.convert_string_to_endpoint(i, Constants.N)
            A_i[MathUtil.convert_2D_coord_to_1D_coord(*endpoint_1)] = 1
            A_i[MathUtil.convert_2D_coord_to_1D_coord(*endpoint_2)] = -1
        
            S_i = np.zeros((Constants.s, 1))
            S_i[i] = 1
            L = L + Constants.k * A_i * A_i.T
            J = J + Constants.k * A_i * S_i.T
    
        return np.kron(L, np.eye(3)), np.kron(J, np.eye(3))
